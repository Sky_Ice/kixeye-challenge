﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum NotificationType
{
    Message,
    Warning,
    Error,
}
/// <summary>
/// Class to handle some notification from server and showing notice to client
/// </summary>
public class NotificationComponent : MonoBehaviour
{
    [SerializeField]
    private Text showingText;

    [SerializeField]
    private Transform container;

    public static NotificationComponent Instance
    {
        get
        {
            if(instance == null)
            {
                Debug.LogError("Notification Component not found!");
                return null;
            } // if
            return instance;
        } // get
    } //
    private static NotificationComponent instance;
    private void Awake()
    {
        instance = this;
    }

    private void createNotification(NotificationType type, string message)
    {
        Text item = GameObject.Instantiate<Text>(showingText, container);
        item.gameObject.SetActive(true);
        item.color = type == NotificationType.Error ? Color.red : type == NotificationType.Message ? Color.yellow : Color.white;
        item.text = message;
    }

    public static void ShowNotification(NotificationType type, string message)
    {
        if(instance != null)
        {
            instance.createNotification(type, message);
        }
    } // ShowNotification
}
