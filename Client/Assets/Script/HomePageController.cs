﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePageController : MonoBehaviour
{

    // Main page
    [SerializeField]
    private Text userLoginName;

    [SerializeField]
    private GameObject selectRoleObject;
    [SerializeField]
    private GameObject userLoginInfor;
    [SerializeField]
    private GameObject adminLoginInfor;


    //For Admin
    [SerializeField]
    private InputField adminUserName;
    [SerializeField]
    private InputField adminPassword;


    private LeaderBoardChallenge parentController;

    public void OnShowing(LeaderBoardChallenge parent)
    {
        parentController = parent;
        OnShowRoles();


    } // OnShowing ()


    private void OnDestroy()
    {
        LCNetwork.CloseSocket();
    }

    #region SELECT ROLES

    public void OnShowRoles()
    {
        selectRoleObject.SetActive(true);
        userLoginInfor.SetActive(false);
        adminLoginInfor.SetActive(false);
    }
    public void OnUserClick()
    {
        selectRoleObject.SetActive(false);
        userLoginInfor.SetActive(true);
        adminLoginInfor.SetActive(false);

    }

    public void OnAdminClick()
    {
        selectRoleObject.SetActive(false);
        userLoginInfor.SetActive(false);
        adminLoginInfor.SetActive(true);
    }
    #endregion

    #region USER
    /// <summary>
    ///  Handle User Login Click
    /// </summary>
    public void OnUserLoginClick()
    {
        if (userLoginName == null || string.IsNullOrEmpty(userLoginName.text))
        {
            NotificationComponent.ShowNotification(NotificationType.Error, "Empty UserLogin");
            //Debug.LogError("Empty UserLogin");
            return;
        } // if

        LCNetwork.Login(userLoginName.text, (res) =>
        {
            Debug.LogWarning("login success : " + res.userName + "===" + res.score);
            finishedLogin(res);
        });

    } // OnUserLoginClick ()

    /// <summary>
    /// Handle User Signup Click ()
    /// </summary>
    public void OnUserSignupClick()
    {
        if (userLoginName == null || string.IsNullOrEmpty(userLoginName.text))
        {
            NotificationComponent.ShowNotification(NotificationType.Error, "Empty UserLogin");
            //Debug.LogError("Empty UserLogin");
            return;
        } // if

        LCNetwork.Signup(userLoginName.text, finishedLogin);
    }

    /// <summary>
    /// Handle response after login
    /// </summary>
    /// <param name="response"></param>
    private void finishedLogin(UserData response)
    {
        // connect to Socket IO here

        
        LCNetwork.ConnectSocket(() =>
        {
            LCNetwork.JoinRoom(response.userName, () =>
            {
                Debug.LogWarning("finish join socket");
                parentController.ShowUserPanel(response);
            });

        });
    } // finishedLogin ()

    private void test()
    {
        LCNetwork.TestEmit("hello");
    }
    #endregion

    #region ADMIN
    public void OnBackClick()
    {
        OnShowRoles();
    }

    public void OnAdminLogin()
    {
        LCNetwork.AdminLogin(adminUserName.text, adminPassword.text, (res) =>
        {
            string token = res.token.Replace('"', new char());
            parentController.ShowAdminPanel(res.token);
        });
    } // OnAdminLogin ()
    #endregion

}
