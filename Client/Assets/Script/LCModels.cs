﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Test
{
    public int updateCount;
    public string userName;
}

[Serializable]
public class UserData
{
    public string userName;
    public int score;
    public int updateCount;
}
public class UserLoginResponse
{
    public string error;
    public UserData data;
}

public class AdminLoginResponse
{
    public string token;
}

public class UpdateScoreResponse
{
    public string error;
    public UserData data;
}
