﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserItem : MonoBehaviour
{
    
    [SerializeField]
    private Text userNameText;
    [SerializeField]
    private Text userscoreText;
    [SerializeField]
    private Text updateCountText;

    private AdminPageController adminPageController;

    public void Init(AdminPageController controller, string userName, string score, string updateCount)
    {
        adminPageController = controller;
        userNameText.text = userName;
        userscoreText.text = score;
        updateCountText.text = updateCount;
    } // Init(){


    public void DeleteUser()
    {
        //delete this user
        adminPageController.DeleteUser(this, userNameText.text);

    } // DeleteUser ()

} // UserItem ()
