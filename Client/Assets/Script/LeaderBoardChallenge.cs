﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LeaderBoardChallenge : MonoBehaviour
{
        
    [SerializeField]
    private GameObject userGroup;
    [SerializeField]
    private GameObject adminGroup;
    [SerializeField]
    private GameObject homePageGroup;

    private bool isLogin = false;

    private void Start()
    {
        BackToHome();
    }

    public void ShowUserPanel(UserData res)
    {
        homePageGroup.SetActive(false);
        userGroup.SetActive(true);
        adminGroup.SetActive(false);

        userGroup.GetComponent<UserPageController>().OnShowing(this, res);
        isLogin = true;
    }

    public void ShowAdminPanel(string token)
    {
        isLogin = true;
        homePageGroup.SetActive(false);
        userGroup.SetActive(false);
        adminGroup.SetActive(true);
        adminGroup.GetComponent<AdminPageController>().OnShowing(this, token);
    }

    public void BackToHome()
    {
        // logout first
        if(isLogin) 
        {
            //
            isLogin = false;
            LCNetwork.CloseSocket();
        } // if
        homePageGroup.SetActive(true);
        userGroup.SetActive(false);
        adminGroup.SetActive(false);

        homePageGroup.GetComponent<HomePageController>().OnShowing(this);
    }

}
