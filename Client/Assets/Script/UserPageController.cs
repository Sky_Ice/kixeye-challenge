﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class UserPageController : MonoBehaviour
{
    private LeaderBoardChallenge parentController;
    private UserData userloginData;
    Dictionary<SocketResponseType, Action<string>> socketListeners = new Dictionary<SocketResponseType, Action<string>>();


    [Header("User Status")]
    [SerializeField]
    private Text userNameLabel;
    [SerializeField]
    private Text userScoreLabel;
    [SerializeField]
    private Text userStateLabel;

    [Header("User Value")]
    [SerializeField]
    private InputField userScoreInput;

    [Header("Notification")]
    [SerializeField]
    private GameObject sampleNotice;
    [SerializeField]
    private Transform noticeContainer;

    [Header("UserName")]
    [SerializeField]
    private GameObject userNamePanel;
    [SerializeField]
    private InputField newUserNameInput;
    [SerializeField]
    private Button conformBtn;



    public void OnShowing(LeaderBoardChallenge parent, UserData data)
    {

        parentController = parent;
        refreshData(data);
        socketListeners.Clear();

        socketListeners.Add(SocketResponseType.OtherChangeScore, otherChangeScore);
        socketListeners.Add(SocketResponseType.OtherJoin, otherJoin);
        socketListeners.Add(SocketResponseType.OtherLogout, otherLogout);
        LCNetwork.RegisterCallback(socketListeners);

    } // OnShowing ()

    private void OnDisable()
    {
        LCNetwork.ClearCallback(socketListeners);
    }

    private void otherChangeScore(string response)
    {
        UserData userData = JsonUtility.FromJson<UserData>(response);
        GameObject noticeItem =  GameObject.Instantiate(sampleNotice, noticeContainer);
        noticeItem.SetActive(true);
        noticeItem.GetComponent<Text>().text = "User <color=red>" + userData.userName + "</color> just update their score to : <color=green>" + userData.score +"</color>";
        
    }
    private void otherJoin(string response)
    {

    }

    private void otherLogout(string response)
    {

    }

    private void refreshData(UserData data)
    {
        userloginData = data;
        userNameLabel.text = data.userName;
        userScoreLabel.text = data.score.ToString();
        userScoreInput.text = data.score.ToString();
    }
    private void FixedUpdate()
    {

        if (LCNetwork.IsSocketOnline())
        {
            userStateLabel.color = Color.green;
            userStateLabel.text = "ONLINE";
        }else
        {
            userStateLabel.color = Color.red;
            userStateLabel.text = "OFFLINE";
        }
    }


    public void OnUpdateScoreClick()
    {
        int score = int.Parse(userScoreInput.text);
        LCNetwork.UpdateScore(userloginData.userName, score, (res) =>
         {
             refreshData(res);
         });
    }

    // Handle Change User Name
    public void OnChangeUserNameClick()
    {
        userNamePanel.SetActive(true);
        newUserNameInput.text = userloginData.userName;
    }

    public void OnConformClick()
    {
        conformBtn.interactable = false;
        LCNetwork.ChangeUserName(userloginData.userName, newUserNameInput.text, (res) =>
        {
            refreshData(res);
            conformBtn.interactable = true;
            userNamePanel.SetActive(false);
        });
    }

    public void OnCancelClick()
    {
        userNamePanel.SetActive(false);
    }

    public void OnLogoutClick()
    {
        parentController.BackToHome();
    }
}
