﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SocketResponseType
{
    OtherJoin,
    OtherChangeName,
    OtherChangeScore,
    OtherLogout,

}
public enum LCResultCode
{
    OK = 200,
    CreateSuccess =201,
    DeleteSuccess = 204,
    NotFound,
}
public class LCDefines
{
    public const string BASE_HTTP_SERVER = "http://127.0.0.1:3000/";
    public const string SOCKET_SERVER_URL = "http://127.0.0.1:3000/socket/";
    public const string SOCKET_NAMESPACE = "/subscribe";
    
}
