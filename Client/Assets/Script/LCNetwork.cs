﻿using BestHTTP;
using BestHTTP.JSON;
using BestHTTP.SocketIO;
using BestHTTP.SocketIO.Transports;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class LCNetwork
{

    #region HTTP

    //defind all request here

    /// <summary>
    /// Login API
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="callback"></param>
    public static void Login(string userName, Action<UserData> callback)
    {
        string fullUrl = LCDefines.BASE_HTTP_SERVER + "leaderboard/" + userName;
        doConnect(fullUrl, HTTPMethods.Get, new Dictionary<string, object>(), (response) =>
        {

            if(response == null)
            {
                NotificationComponent.ShowNotification(NotificationType.Error, " Login Error : ");
                //Debug.LogError("login error : ");
                return;
            } // if
            Debug.LogWarning("response : " + response);
            UserData res = JsonUtility.FromJson<UserData>(response);
            callback(res);
        });
    }

    /// <summary>
    /// Signup API
    /// </summary>
    /// <param name="response"></param>
    public static void Signup(string userName, Action<UserData> callback)
    {

        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("userName", userName);
        
        doConnect(LCDefines.BASE_HTTP_SERVER + "leaderboard", HTTPMethods.Post, param, (response) =>
        {

            Debug.LogWarning("response : " + response);
            UserData res = JsonUtility.FromJson<UserData>(response);
            callback(res);
        });
    }


    /// <summary>
    /// Change Score API
    /// </summary>
    /// <param name="userName"></param>
    /// <param name="newScore"></param>
    /// <param name="callback"></param>
    public static void UpdateScore(string userName, int newScore, Action<UserData> callback)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("score", newScore);
        param.Add("userName", userName);
        doConnect(LCDefines.BASE_HTTP_SERVER + "leaderboard/score", HTTPMethods.Put, param, (response) =>
        {
            Debug.LogWarning("response : " + response);
            UserData res = JsonUtility.FromJson<UserData>(response);
            callback(res);
        });
    }

    public static void ChangeUserName(string userName, string newUserName, Action<UserData> callback)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("newUserName", newUserName);
        param.Add("userName", userName);
        doConnect(LCDefines.BASE_HTTP_SERVER + "leaderboard/username", HTTPMethods.Put, param, (response) =>
        {
            Debug.LogWarning("response : " + response);
            UserData res = JsonUtility.FromJson<UserData>(response);
            callback(res);
        });
    }

    public static void TestData()
    {
        doConnect(LCDefines.BASE_HTTP_SERVER, HTTPMethods.Get, new Dictionary<string, object>(), (response) =>
        {
            Debug.LogWarning("response : " + response);
        });
    }


    //=====ADMIN API

    public static void AdminLogin(string userName, string password,Action<AdminLoginResponse> callback)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("userName", userName);
        param.Add("password", password);
        doConnect(LCDefines.BASE_HTTP_SERVER + "login", HTTPMethods.Post, param, (response) =>
        {
            Debug.LogWarning("admin login : " + response);
            AdminLoginResponse res = JsonUtility.FromJson<AdminLoginResponse>(response);
            callback(res);
        });
    }

    public static void GetAllUserByAdmin (string token, Action<List<UserData>> callback)
    {

    }

    public static void GetUserByTimes(string token, long startTime, long endTime, Action<List<UserData>> callback)
    {

        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("start", startTime);
        param.Add("end", endTime);
        doConnect(LCDefines.BASE_HTTP_SERVER + "leaderboard/admin/list", HTTPMethods.Get, param, (response) =>
        {
            Debug.LogWarning("admin get user : " + response);
            
            UserData[] res = JsonHelper.getJsonArray<UserData>(response);

            //AdminLoginResponse res = JsonUtility.FromJson<AdminLoginResponse>(response);
            callback(res.ToList());
        }, token);
    }

    public static void DeleteUserByAdmin (string token, string userName, Action callback)
    {
        Dictionary<string, object> param = new Dictionary<string, object>();
        param.Add("userName", userName);
        doConnect(LCDefines.BASE_HTTP_SERVER + "leaderboard/admin", HTTPMethods.Delete, param, (response) =>
        {
            callback();
        }, token);
    }


    public static void GetUserByAdmin(string token, string userName, Action<UserData> callback)
    {
        string fullURL = LCDefines.BASE_HTTP_SERVER + "leaderboard/admin/user/" + userName;

        doConnect(fullURL, HTTPMethods.Get, new Dictionary<string, object>(), (response) =>
        {
            Debug.LogWarning("admin get user : " + response);

            UserData data = JsonUtility.FromJson<UserData>(response);
            callback(data);
        }, token);
    }





    // ========= PRIVATE HELPER
    private static string getFullURL(string path)
    {
        return "" + path;
    }
    private static void doConnect(string path, HTTPMethods methodType, Dictionary<string,object> requestParams, Action<string> callback, string token = "")
    {
        string fullURL = getFullURL(path);
        Debug.LogWarning("connect to : " + fullURL);
        var request = new HTTPRequest(new Uri(fullURL), (req, res) => {
            onRequestCallBack(req, res, callback);
        });

        if (!string.IsNullOrEmpty(token))
        {
            request.AddHeader("Authorization", token);
        }

        //add Header 
        //string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("milu2_admin" + ":" + "mlml246"));
        //request.AddHeader("Authorization", "Basic " + svcCredentials);


        request.AddHeader("Content-Type", "application/json");

        string stringParams = Json.Encode(requestParams);
        request.RawData = new System.Text.UTF8Encoding().GetBytes(stringParams);
       
        request.MethodType = methodType;
        request.Send();

    } // doConnect ()

    private static void onRequestCallBack(HTTPRequest request, HTTPResponse response, Action<string> callback)
    {

        LCResultCode resultCode = (LCResultCode)response.StatusCode;
        if (resultCode != LCResultCode.OK && resultCode != LCResultCode.CreateSuccess && resultCode != LCResultCode.DeleteSuccess)
        {
            NotificationComponent.ShowNotification(NotificationType.Error, "Request Error : " + response.StatusCode + "===" + response.DataAsText);
            //Debug.LogError("show error : " + resultCode + "==="+ response.DataAsText);
        }
        else
        {
            callback(response.DataAsText);
        } // else

    } // onRequestCallBack 

    #endregion


    #region SOCKET IO

    private static SocketManager socketManager;
    private static Socket socket;

    private static Action connectedCallback;
    private static Action joinRoomCallback;
    private static Dictionary<SocketResponseType, Action<string>> socketListeners = new Dictionary<SocketResponseType, Action<string>>();

    public static void CloseSocket()
    {
        if(socketManager != null && socketManager.State != SocketManager.States.Closed)
        {
            socketManager.Close();
        }
    }

    public static void ConnectSocket(Action callback)
    {
        Debug.LogWarning("begin connect socket : ");
        connectedCallback = callback;
        if(socketManager == null)
        {
            SocketOptions option = new SocketOptions();
            option.Timeout = TimeSpan.FromSeconds(40.0f);
            option.AutoConnect = false;
            option.Reconnection = false;
            //option.ConnectWith = TransportTypes.WebSocket;
            socketManager = new SocketManager(new Uri(LCDefines.SOCKET_SERVER_URL), option);
        }

        socket = socketManager.GetSocket(LCDefines.SOCKET_NAMESPACE);
        socket.On("connect", onConnected);
        socket.On("disconnect", onDisconnected);
        socket.On(SocketIOEventTypes.Error, onError);
        //socket.On("error", onError);
        socket.On("resp", onResponse);
        socket.On("joinfinished", onJoinFinished);
        socket.On("updatescore", onUpdateScoreFinished);
        //socket.On("test", onTestResponse);
        socketManager.Open();
    } // Connect ()


    public static void RegisterCallback(Dictionary<SocketResponseType, Action<string>> listeners)
    {
        foreach(var listener in listeners)
        {
            if (socketListeners.ContainsKey(listener.Key))
            {
                socketListeners[listener.Key] = listener.Value;
            } else
            {
                socketListeners.Add(listener.Key, listener.Value);
            } // else
        } // foreach
    } // RegisterCallback

    public static void ClearCallback(Dictionary<SocketResponseType, Action<string>> listeners)
    {
        foreach(var listener in listeners)
        {
            socketListeners.Remove(listener.Key);
        }
    } // ClearCallback()


    //===== Base handle connection state

    private static void onError(Socket socket, Packet packet, params object[] args)
    {
        Debug.LogWarning("on error : " + socket.Id);
    }

    private static void onDisconnected(Socket socket, Packet packet, params object[] args)
    {
        Debug.LogWarning("on disconnected : " + socket.Id);
        //connectedCallback();
    }

    private static void onConnected(Socket socket, Packet packet, params object[] args)
    {
        Debug.LogWarning("on connected : " + socket.Id);
        connectedCallback();
    }

    private static void onResponse(Socket socket, Packet packet, params object[] args)
    {
        Debug.LogWarning("on response : " + args);
    }

    private static void onJoinFinished(Socket socket, Packet packet, params object[] args)
    {
        joinRoomCallback();
    }


    private static void onUpdateScoreFinished(Socket socket, Packet packet, params object[] args)
    {
        string strdata = Json.Encode(args[0]);
        Debug.LogWarning("other update score : " + strdata);
        Action<string> callback = null;
        if(socketListeners.TryGetValue(SocketResponseType.OtherChangeScore, out callback))
        {
            callback(strdata);
        }
    }

    //==== PUBLIC CALL

    public static void JoinRoom(string userName, Action callback)
    {
        // join room
        socket.Emit("join", userName);
        joinRoomCallback = callback;
        //try to expain time out length
        
        Debug.LogWarning("time out : " + socketManager.Handshake.PingInterval + "===" + socketManager.Handshake.PingTimeout);
    }



    public static bool IsSocketOnline()
    {
        if(socket == null)
        {
            return false;
        } // if
        return socket.IsOpen;
    }



    public static void TestEmit(string msg)
    {
        socket.Emit("test", msg);
    }
#endregion
}
