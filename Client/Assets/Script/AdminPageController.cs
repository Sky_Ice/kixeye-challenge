﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AdminPageController : MonoBehaviour
{
    private string adminToken;
    private LeaderBoardChallenge parentPage;

    [SerializeField]
    private UserItem sampleItem;
    [SerializeField]
    private Transform itemContainer;

    // Find User
    [SerializeField]
    private GameObject findUserPanel;
    [SerializeField]
    private InputField userNameInput;


    // Find Time
    [SerializeField]
    private GameObject findTimePanel;
    [SerializeField]
    private InputField startTimeInput;
    [SerializeField]
    private InputField endTimeInput;
    [SerializeField]
    private GameObject datepicker;
    private InputField currentSelect;
    private long startTimeTick;
    private long endTimeTick;

    public void OnShowing(LeaderBoardChallenge parentPage, string token)
    {
        findUserPanel.SetActive(false);
        findTimePanel.SetActive(false);

        this.parentPage = parentPage;
        adminToken = token;

        //get all list item
        /*
        List<UserData> test = new List<UserData>();
        for(int i = 0; i < 100; i++)
        {
            test.Add(new UserData { userName = i.ToString(), score = i, updateCount = i });
        }
        showingItems(test);
        */

    }

    private void showingItems(List<UserData> datas)
    {
        clearAllUserItem();
        foreach(var data in datas)
        {
            showingItem(data);
        } // foreach
    }

    private void showingItem(UserData data)
    {
        UserItem item = GameObject.Instantiate<UserItem>(sampleItem, itemContainer);
        item.gameObject.SetActive(true);
        item.Init(this, data.userName, data.score.ToString(), data.updateCount.ToString());
    }

    private void clearAllUserItem()
    {
        for(int i =0; i <itemContainer.childCount; i++)
        {
            GameObject.Destroy(itemContainer.GetChild(i).gameObject);
        }
    }

    public void BackClick()
    {
        parentPage.BackToHome();
    }

    public void DeleteUser(UserItem item, string userName)
    {
        //delete
        LCNetwork.DeleteUserByAdmin(adminToken, userName, ()=> {
            GameObject.Destroy(item.gameObject);
        });
    }

    public void FindUserClick()
    {
        findUserPanel.SetActive(true);
    }

    public void FindUserConformClick()
    {
        LCNetwork.GetUserByAdmin(adminToken, userNameInput.text, (res) =>
        {
            clearAllUserItem();
            showingItem(res);
            findUserPanel.SetActive(false);
        });
    }

    public void FindUserCancelClick()
    {
        findUserPanel.SetActive(false);
    }

    public void FindTimeClick()
    {
        findTimePanel.SetActive(true);
    }

    public void FindTimeConformClick()
    {
        LCNetwork.GetUserByTimes(adminToken, startTimeTick, endTimeTick, (res) =>
        {
            showingItems(res);
            findTimePanel.SetActive(false);
        });
    }

    public void FindTimeCancelClick()
    {
        findTimePanel.SetActive(false);
    }


    public void OnStartSelect()
    {
        datepicker.SetActive(true);
        currentSelect = startTimeInput;
    }

    public void OnEndSelect()
    {
        datepicker.SetActive(true);
        currentSelect = endTimeInput;
    }

    public void SelectTime()
    {
        if(currentSelect == startTimeInput)
        {
            startTimeTick = DatePickerControl.DateGlobal.Ticks;
        } else
        {
            endTimeTick = DatePickerControl.DateGlobal.Ticks;
        }
        currentSelect.text = DatePickerControl.DateGlobal.ToString();
        datepicker.SetActive(false);

    }
}
