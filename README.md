# KixEye Challenge
Leader service challenge that could be consumed by a game client.  This service will be run on a remote server from the client.

##Installation

###Database Installing
KixEye Challenge using MongoDB which can be download in here : https://www.mongodb.com/download-center/community
Let using simple default setting is enough for project running. Using MongoDB Compass Community to access and manage database.


###Server Installing
Please install newest version of NodeJS to avoid syntax error which can be occurred in some old version. Current version is 10.15.2
Move to server directory and install Node modules

```
npm install
```

###Client Installing

Unity version : 2018.3.0 which can be download in here : https://unity3d.com/get-unity/download/archive?_ga=2.248649223.1437320902.1551535661-1419087510.1509521729

If you dont want using Unity Editor as your client side, exe build file also can be found in Build folder.
