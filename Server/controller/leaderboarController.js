
/**
 * Module Dependencies
 */
const errors = require('restify-errors');
const app = require('../index');
const _ = require('underscore');
const config = require('../config');
const crypt = require('../Authen/Crypt');
const adminInfor = require('../adminConfig');
/**
 * Model Schema
 */
const LeaderBoardModel = require('../models/leaderboardModel');
const LeaderBoardHisModel = require('../models/leaderboardHistoryModel');
const modelReturn = { "userName": 1, "score": 1, "updateCount": 1, "_id": 0 };

function leaderboardHistory(data, callback) {
    let history = new LeaderBoardHisModel(data);
    history.save(callback);
}

module.exports = {
	/**
	 * POST
	 */
    Create: (req, res, next) => {

        // if (!req.is('application/json')) {
        //     return next(
        //         new errors.InvalidContentError("Expects 'application/json'"),
        //     );
        // }




        let data = req.body || {};

        console.log("create API data : " + req.body);
        let leaderboard = new LeaderBoardModel(data);
        leaderboard.save(function (err, obj) {
            if (err) {
                console.error(err);
                return next(new errors.InternalError(err.message));
            } // if
            leaderboardHistory({ userName: obj.userName, userId: obj._id, score: obj.score });
            res.send(201, data);
            next();
        });
    },

	/**
	 * LIST
	 */
    Getall: (req, res, next) => {
        LeaderBoardModel.apiQuery(req.params, function (err, docs) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errors.name.message),
                );

            } // if

            res.send(docs);
            next();
        });
    },

	/**
	 * GET
	 */
    GetByUserName: (req, res, next) => {
        LeaderBoardModel.findOne({ userName: req.params.userName }, modelReturn, function (err, doc) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            }
            if (doc == null) {
                return next(
                    new errors.InvalidContentError("User Name not found"),
                );
            }
            res.send(doc);
            next();
        });
    },


	/**
	 * UPDATE
	 */
    /*
    Update: (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'"),
            );
        }

        let data = req.body || {};

        if (!data._id) {

            data = Object.assign({}, data, { userName: req.body.userName });
        }

        LeaderBoardModel.findOne({ userName: req.body.userName }, function (err, doc) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            } else if (!doc) {
                return next(
                    new errors.ResourceNotFoundError(
                        'The resource you requested could not be found.',
                    ),
                );
            }
            data._id = doc._id;
            LeaderBoardModel.update({ _id: data._id }, data, function (err) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errmsg),
                    );
                }

                res.send(200, data);
                next();
            });
        });
    },
    */

    /**
     * CHANGE USER NAME
     */
    ChangeUserName: (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'"),
            );
        }

        LeaderBoardModel.findOne({ userName: req.body.userName }, function (err, doc) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            } else if (!doc) {
                return next(
                    new errors.ResourceNotFoundError(
                        'The resource you requested could not be found.',
                    ),
                );
            }
            let data = req.body || {};
            data.userName = req.body.newUserName;
            data.score = doc.score;

            LeaderBoardModel.updateOne({ _id: doc.id }, data, function (err, result) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errmsg),
                    );
                }
                if (result.ok != 1) {
                    return next(
                        new errors.InvalidContentError(
                            'Update failed',
                        ),
                    );
                }

                leaderboardHistory({ userId: doc.id, userName: data.userName, score: data.score });

                //push to other client
                _.each(app.socketSV.members, (socket) => {
                    socket.emit("changeusername", data);
                    console.log(socket);

                });
                //response to client
                res.send(200, data);
                next();
            });
        });

    },

    /**
     * UPDATE SCORE 
     */

    UpdateScore: (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'"),
            );
        }

        LeaderBoardModel.findOne({ userName: req.body.userName }, function (err, doc) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            } else if (!doc) {
                return next(
                    new errors.ResourceNotFoundError(
                        'The resource you requested could not be found.',
                    ),
                );
            }
            let data = {};
            data.score = req.body.score;
            //insert udpate score by 1
            data.updateCount = doc.updateCount + 1;
            data.userName = req.body.userName;

            LeaderBoardModel.updateOne({ _id: doc.id }, data, function (err, result) {
                if (err) {
                    console.error(err);
                    return next(
                        new errors.InvalidContentError(err.errmsg),
                    );
                }
                if (result.ok != 1) {
                    return next(
                        new errors.InvalidContentError(
                            'Update failed',
                        ),
                    );
                }

                //save to history
                leaderboardHistory({ userId: doc.id, userName: doc.userName, score: data.score });
                //push to other client
                _.each(app.socketSV.members, (socket) => {
                    socket.emit("updatescore", data);
                    console.log(socket);

                });


                //response to client
                res.send(200, data);
                next();
            });
        });

    },


	/**
	 * DELETE
	 */
    Delete: (req, res, next) => {
        let q = LeaderBoardModel.deleteOne({ userName: req.params.userName }, function (err) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            }

            res.send(204);
            next();
        });
    },


    /**
     * Login
     */
    Login: (req, res, next) => {
        if (!req.is('application/json')) {
            return next(
                new errors.InvalidContentError("Expects 'application/json'"),
            );
        }

        if (req.body.userName != config.adminUser || req.body.password != config.adminPass) {
            return next(
                new errors.InvalidContentError("wrong username or password"),
            );
        }

        adminInfor.admin_agent = req.headers['user-agent'];
        adminInfor.admin_login_time = Date.now();
        let token = crypt.encrypt(`${req.body.userName}|${req.headers['user-agent']}|${adminInfor.admin_login_time}`)
        console.log(token);
        //console.log(crypt.decrypt(token))
        res.send({token: token});
        next();
    },
    /**
     * Admin Get
     */
    AdminGetAll: (req, res, next) => {
        let perPage = 10;
        let page = req.body["page"] || 1;
        let start = parseFloat(req.body["start"]);
        let end = parseFloat(req.body["end"]);
        let query = {}
        start = new Date(start).getTime() || 0;
        end = new Date(end).getTime() || 0;
        //start and end time valid
        if (start < end && start > 0) {
            query = { updatedAt: { $gte: new Date(start), $lt: new Date(end) } };
        } else if (start > 0 && start > end) { //only start time valid
            query = { updatedAt: { $gte: new Date(start) } };
        } else if (start == 0 && end > 0) { // only end time valid
            query = { updatedAt: { $lt: new Date(end) } };
        } else {

        }
        LeaderBoardHisModel.find(query, null, { limit: perPage, skip: perPage * (page - 1) }, function (err, docs) {
            console.log(docs)

            let result = {}
            _.each(docs, value => {
                
                // console.log(value);
                if(_.has(result,value.userId)){
                    result[value.userId].updateCount +=1;
                    result[value.userId].userName = value.userName;
                    result[value.userId].score = value.score;
                } else {
                    // console.log("add to : " + value.userId);
                    let data = {}
                    data.updateCount = 1;
                    result[value.userId] = data;
                    result[value.userId].userName = value.userName;
                    result[value.userId].score = value.score;
                }
            });


            // _.each(result, r=>{

            //     console.log(r);
            //     LeaderBoardModel.findById(r._id, function (err, doc) {
            //         if (err) {
            //             console.error(err);
            //             return next(
            //                 new errors.InvalidContentError(err.errmsg),
            //             );
            //         } else if (!doc) {
            //             return next(
            //                 new errors.ResourceNotFoundError(
            //                     'The resource you requested could not be found.',
            //                 ),
            //             );
            //         }
            //         r.userName = doc.userName;
            //     });

            // });
            // console.log(_.values(result));
            // res.send({data:_.values(result)});
            res.send(_.values(result));
             next();
        })
        next();

    },

    AdminGetUserName: function (req, res, next) {
        LeaderBoardModel.findOne({ userName: req.params.userName }, function (err, doc) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            }
            if (doc == null) {
                return next(
                    new errors.InvalidContentError("User Name not found"),
                );
            }
            res.send(doc);
            next();
        });
    },

    AdminDeleteUser: function (req, res, next) {
        LeaderBoardModel.deleteOne({ userName: req.body.userName }, function (err, result) {
            if (err) {
                console.error(err);
                return next(
                    new errors.InvalidContentError(err.errmsg),
                );
            }
            if (result.ok != 1) {
                return next(
                    new errors.InvalidContentError(
                        'Delete failed',
                    ),
                );
            }
            res.send();
        })
    }
};    