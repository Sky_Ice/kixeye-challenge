/**
 * Module Dependencies
 */
const config = require('./config');
const restify = require('restify');
const mongoose = require('mongoose');
const restifyPlugins = require('restify-plugins');
const _ =require('underscore');

/**
  * Initialize Server
  */
const server = restify.createServer({
	name: config.name,
	version: config.version,
});

/**
  * Middleware
  */
server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());

/**
  * Start Server, Connect to DB & Require Routes
  */
server.listen(config.port, () => {
	// establish connection to mongodb
	// mongoose.Promise = global.Promise;
	mongoose.connect(config.db.uri, { useNewUrlParser: true,  useCreateIndex: true, });

	const db = mongoose.connection;

	db.on('error', (err) => {
	    console.error(err);
	    process.exit(1);
	});

	db.once('open', () => {
	    require('./routes/index')(server);
	    console.log(`Server is listening on port ${config.port}`);
	});
});


/**
 * Socket IO
 */
let opt = {
	path: "/socket",
	serveClient: true,
	pingTimeout:75000,
	transports: ['polling', 'websocket']
}
const socketSV = require('socket.io').listen(server.server,opt).of('/subscribe');
socketSV.members= {}; // username== socket
socketSV.on('connection', (socket) => {
		console.log(socket.id);
		socket.emit("connected", socket.id);
		// socketSV.emit("resp", "someone connected : " + socket.id);
    // socket.on("m", (mess) => {
    //     // console.log(mess);
		// })
		// socket.on("test", (mess)=>{
		// 	console.debug("test message : " + mess);
		// })
		
		socket.on("join", (mess) => {
				// some one join , save to list
				console.log("client : " + mess + "== join");
				
				if(_.has(socketSV.members,mess)){
					//disconnected old socket
					console.log("disconnect : " + socketSV.members[mess].id);
					socketSV.members[mess].disconnect();
				} // if
				console.log(_.has(socketSV.members,mess));
				socketSV.members[mess] = socket;
				socket.emit("joinfinished");
		})


})

socketSV.on('disconnect',(socket) => {
	console.log("disconnect : " + socket.id);
})

//temp
module.exports.socketSV = socketSV;