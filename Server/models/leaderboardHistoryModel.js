const mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

const LeaderboarHistorySchema = new mongoose.Schema(
    {
        userId:{
            type: String,
            required: true           
        },
        userName: {
            type: String,
            required: true,
            trim: true           
        },
        score: {
            type: Number,
            required: true,
            default: 0
        }
    },
    { minimize: true },
);

LeaderboarHistorySchema.plugin(timestamps);
LeaderboarHistorySchema.plugin(mongooseStringQuery);
// add custom function for this steam

const LeaderBoard = mongoose.model('leaderboardsHistory', LeaderboarHistorySchema);
module.exports = LeaderBoard;  