const mongoose = require('mongoose');
const mongooseStringQuery = require('mongoose-string-query');
const timestamps = require('mongoose-timestamp');

const LeaderboarSchema = new mongoose.Schema(
    {
        userName: {
            type: String,
            required: true,
            trim: true,
            unique: true
        },
        score: {
            type: Number,
            required: true,
            default: 0
        },        
        updateCount: {
            type: Number,
            required: false,
            default: 0            
        }
    },
    { minimize: true },
);

LeaderboarSchema.plugin(timestamps);
LeaderboarSchema.plugin(mongooseStringQuery);
// add custom function for this steam

const LeaderBoard = mongoose.model('leaderboards', LeaderboarSchema);
module.exports = LeaderBoard;  