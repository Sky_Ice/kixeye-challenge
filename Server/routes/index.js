const LeaderBoard = require('../controller/leaderboarController');
const adminInfo = require('../adminConfig');
const Crypt = require('../Authen/Crypt');
const config = require('../config');

module.exports = function (server) {

    //check login state

    server.use((req, res, next) => {
        //check if url is admin page
        if (/.*admin.*/.test(req.url)) {
            //fail without token
            if (!req.headers.authorization) {
                res.send("Authen Fail");
                return;
            } else {
                //calculate token
                let tokenInfor = Crypt.decrypt(req.headers.authorization);
                let infor = tokenInfor.split("|");
                //wrong user
                if (infor[0] != config.adminUser || infor[1] != req.headers["user-agent"] || infor[2] != adminInfo.admin_login_time) {
                    res.send(401);
                    return;
                }
            }
            next();
        } else {
            next();
        }

    })

    server.get('/', (req, res, next) => {
        res.send('Welcome');
        next();
    })


    /**
	 * POST
	 */
    server.post('/leaderboard', LeaderBoard.Create);

    server.get('/leaderboard', LeaderBoard.Getall);

    server.get('/leaderboard/:userName', LeaderBoard.GetByUserName);

    server.del('/leaderboard/admin', LeaderBoard.Delete);


    // server.put('/leaderboard', LeaderBoard.Update)

    server.put('/leaderboard/username', LeaderBoard.ChangeUserName);
    server.put('/leaderboard/score', LeaderBoard.UpdateScore);

    server.post('/login', LeaderBoard.Login);

    server.get('/leaderboard/admin/list', LeaderBoard.AdminGetAll)
    server.get('/leaderboard/admin/user/:userName', LeaderBoard.AdminGetUserName)

}